package com.kenfogel.timestamp_demo;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * This is a demonstration on how to work with a Timestamp that only needs the
 * date without a time component in an FXML form with bi-directional binding
 *
 * @author Ken Fogel
 * @version 1.0
 * @date 2017-06-26
 */
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    @Override
    public void start(Stage primaryStage) {
        try {
            // Instantiate a FXMLLoader object
            FXMLLoader loader = new FXMLLoader();

            // Connect the FXMLLoader to the fxml file that is stored in the jar
            loader.setLocation(MainApp.class
                    .getResource("/fxml/TimestampForm.fxml"));

            // The load command returns a reference to the root pane of the fxml file
            Parent root = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(root);

            // Put the Scene on the Stage
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException ex) {
            LOG.error("Failed to load FXML file", ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
