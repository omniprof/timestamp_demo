package com.kenfogel.timestamp_demo.controller;

import com.kenfogel.timestamp_demo.bean.TimestampBean;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This simple form uses a DatePicker control that is bound to an ObjectProperty
 * that contains a LocalDate.
 *
 * @author Ken Fogel
 * @version 1.0
 * @date 2017-06-26
 */
public class TimestampFormController {

    private final static Logger LOG = LoggerFactory.getLogger(TimestampFormController.class);

    private final TimestampBean tsb;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="datePickerField"
    private DatePicker datePickerField; // Value injected by FXMLLoader

    /**
     * Constructor that creates the TimestampBean for this example
     */
    public TimestampFormController() {
        tsb = new TimestampBean();
    }

    /**
     * Event handler for Display the Property button. When pressed it displays
     * as a String the LocalDate stored in the dateField property
     *
     * @param event
     */
    @FXML
    void onDisplayProperty(ActionEvent event) {
        LOG.info("The value in the bean is " + tsb.getDateField().toString());
    }

    /**
     * Standard exit
     *
     * @param event
     */
    @FXML
    void onExit(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Event handler for Set the Property button. When pressed it sets the
     * dateField property to an arbitrary date that should immediately appear on
     * the display.
     *
     * @param event
     */
    @FXML
    void onSetProperty(ActionEvent event) {
        LOG.debug("Reset date to 2015-05-18");
        tsb.setDateField(LocalDate.parse("2015-05-18"));
    }

    /**
     * Bind the DatePicker to the dateField property. No converter is required.
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        Bindings.bindBidirectional(datePickerField.valueProperty(), tsb.dateFieldProperty());
    }
}
